import numpy as np
from flask import Flask, render_template, request
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
app = Flask(__name__)
import requests
import mpld3



def get_countries():
    url = "https://covid-193.p.rapidapi.com/countries"

    headers = {
        "X-RapidAPI-Key": "71c2924226msh99094eb99fece8cp1c55fcjsn2547196cefb3",
        "X-RapidAPI-Host": "covid-193.p.rapidapi.com"
    }
    response = requests.get(url, headers=headers)

    if response.status_code == 200:
        data = response.json()
        country_names = data.get('response', [])
        return country_names
    else:
        print(f"Error: {response.status_code}")
        return []


def get_statistics():
    url = "https://covid-193.p.rapidapi.com/statistics"

    headers = {
        "X-RapidAPI-Key": "71c2924226msh99094eb99fece8cp1c55fcjsn2547196cefb3",
        "X-RapidAPI-Host": "covid-193.p.rapidapi.com"
    }

    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        data = response.json()
        lista = data.get('response', [])
        return lista
    else:
        print(f"Error: {response.status_code}")
        return []


def get_countries_codes_and_names():
    url = "https://covid-19-statistics.p.rapidapi.com/regions"

    headers = {
        "X-RapidAPI-Key": "71c2924226msh99094eb99fece8cp1c55fcjsn2547196cefb3",
        "X-RapidAPI-Host": "covid-19-statistics.p.rapidapi.com"
    }

    response = requests.get(url, headers=headers)

    if response.status_code == 200:
        data = response.json()
        countries_data = data.get('data', [])
        sorted_countries = sorted(countries_data, key=lambda x: x.get('iso'))
        country_codes_and_names = [f"{country.get('iso')} - {country.get('name')}" for country in sorted_countries]
        return country_codes_and_names
    else:
        print(f"Error: {response.status_code}")
        return None



def get_countries_and_iso():
    url = "https://covid-19-statistics.p.rapidapi.com/regions"

    headers = {
        "X-RapidAPI-Key": "82849bd050msh252549694dfdf02p12635ajsne577ad4dce21",
        "X-RapidAPI-Host": "covid-19-statistics.p.rapidapi.com"
    }

    response = requests.get(url, headers=headers)

    data = response.json()
    names = [country['name'] for country in data['data']]
    iso = [country['iso'] for country in data['data']]
    return names, iso


def get_common_countries():
    countries_set_1 = set(get_countries())
    countries_set_2 = set(get_countries_codes_and_names())
    countries_set_2 = [country.split(' - ')[1] for country in countries_set_2]
    common_countries_set = countries_set_1.intersection(countries_set_2)
    common_countries_list = list(common_countries_set)

    return sorted(common_countries_list)

def get_total_cases_per_country(iso, date):
    url = "https://covid-19-statistics.p.rapidapi.com/reports"

    querystring = {"iso": iso, "date": date}

    headers = {
        "X-RapidAPI-Key": "82849bd050msh252549694dfdf02p12635ajsne577ad4dce21",
        "X-RapidAPI-Host": "covid-19-statistics.p.rapidapi.com"
    }

    try:
        response = requests.get(url, headers=headers, params=querystring)
        response.raise_for_status()
        data = response.json()
        confirmed = [confirmed['confirmed_diff'] for confirmed in data['data']]
        sum = np.sum(confirmed)
        return sum
    except requests.exceptions.RequestException as e:
        print(f"An error occurred: {e}")
        return None


def get_total_deaths_per_country(iso, date):
    url = "https://covid-19-statistics.p.rapidapi.com/reports"

    querystring = {"iso": iso, "date": date}

    headers = {
        "X-RapidAPI-Key": "82849bd050msh252549694dfdf02p12635ajsne577ad4dce21",
        "X-RapidAPI-Host": "covid-19-statistics.p.rapidapi.com"
    }

    try:
        response = requests.get(url, headers=headers, params=querystring)
        response.raise_for_status()
        data = response.json()
        deaths = [deaths['deaths_diff']   for deaths in data['data']]
        sum = np.sum(deaths)
        return sum
    except requests.exceptions.RequestException as e:
        print(f"An error occurred: {e}")
        return None

def get_interval_cases(iso, start_date_str, end_date_str):
    start_date = datetime.strptime(start_date_str, "%Y-%m-%d").date()
    end_date = datetime.strptime(end_date_str, "%Y-%m-%d").date()

    interval = []
    sum_confirmed = []
    current_date = start_date

    while current_date <= end_date:
        interval.append(current_date)
        current_date += timedelta(days=1)

    for date in interval:
        sum_confirmed.append(get_total_cases_per_country(iso, date))

    suma1 = np.sum(sum_confirmed)

    plt.tight_layout()
    fig, ax = plt.subplots(figsize=(10, 6))
    ax.plot(interval, sum_confirmed, label='Evolutie imbolnaviri')
    ax.set_xlabel('Data', fontsize=16, fontweight='bold')
    ax.set_ylabel('Imbolnaviri', fontsize=16, fontweight='bold')
    ax.legend(fontsize=12)
    ax.grid(False)

    return mpld3.fig_to_html(fig), suma1


def get_interval_deaths(iso, start_date_str, end_date_str):
    start_date = datetime.strptime(start_date_str, "%Y-%m-%d").date()
    end_date = datetime.strptime(end_date_str, "%Y-%m-%d").date()

    interval = []
    sum_deaths = []
    current_date = start_date

    while current_date <= end_date:
        interval.append(current_date)
        current_date += timedelta(days=1)

    for date in interval:
        sum_deaths.append(get_total_deaths_per_country(iso, date))

    suma2 = np.sum(sum_deaths)

    plt.tight_layout()
    fig, ax = plt.subplots(figsize=(10, 6))
    ax.plot(interval, sum_deaths, label='Evolutie decese')
    ax.set_xlabel('Data', fontsize=16, fontweight='bold')
    ax.set_ylabel('Decese', fontsize=16, fontweight='bold')
    ax.legend(fontsize=12)
    ax.grid(False)
    print(sum_deaths)
    return mpld3.fig_to_html(fig), suma2


@app.route('/', methods=['GET', 'POST'])
def interfata_html():
    countries_2 = get_common_countries()
    names, iso = get_countries_and_iso()
    if request.method == 'POST':
        country = request.form['country']
        start_date = request.form['start_date']
        end_date = request.form['end_date']
        iso = iso[names.index(country)]
        stats = get_statistics()
        fig, confirmed = get_interval_cases(iso, start_date, end_date)
        fig_2, deaths = get_interval_deaths(iso, start_date, end_date)

        return render_template('interfata.html', countries = countries_2, statistics=stats, selected_country=country, html_fig=fig, html_fig2 = fig_2, confirmed = confirmed, deaths = deaths, start_date = start_date, end_date = end_date)

    return render_template('interfata.html', countries=countries_2, statistics=None, selected_country=None, html_fig=None, html_fig2 = None, confirmed = None, deaths = None, start_date = None, end_date = None)


if __name__ == '__main__':
    app.run()


